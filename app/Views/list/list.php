<div class="row d-flex container">
    <div class="col-md-12">
        <div class="card-hover-shadow-2x mb-3 card">
            <div class="card-header-tab card-header" id="card-header">
                <div id="card-title" class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="fa fa-tasks"></i>&nbsp;Lists</div>
            </div>
            <div class="scroll-area-sm">
                <perfect-scrollbar class="ps-show-limits">
                    <div style="position: static;" class="ps ps--active-y">
                        <div class="ps-content" id="content">
                            <ul class="list-group list-group-flush" id="lists">
                                <?php foreach ($lists as $list): ?>
                                    <li class="list-group-item list <?php if ($list['is_done'] == 1): echo "checked"; else: echo ""; endif;?>" listId=<?php echo $list['id'] ?>>
                                        <div class="todo-indicator bg-warning"></div>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-2">
                                                </div>
                                                <div class="widget-content-left" onclick="showList(<?php echo $list['id'] ?>)">
                                                    <div class="widget-heading"><?php echo $list['list_name'] ?>
                                                    </div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <button class="border-0 btn-transition btn btn-outline-danger"
                                                        onclick="deleteList(<?php echo $list['id'] ?>)">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </perfect-scrollbar>
            </div>
            <div id="cardfooter" class="text-right card-footer"><button class="btn btn-primary" id="createlist"></i>Add List</button></div>
        </div>
    </div>
</div>

<script src="/assets/js/showList.js"></script>