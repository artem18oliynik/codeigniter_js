<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\ListModel;

class ProductModel extends Model
{
    protected $table = 'products';
    protected $allowedFields = ['product_name', 'list_id', 'performed'];
    protected $primaryKey = 'id';
    protected $returnType = 'array';

    public function checkPerformedByListId($listId)
    {
        if (!empty($listId)){
            $products = $this->where('list_id', $listId)->findColumn('performed');
            if (!empty($products)){
                foreach ($products as $product) {
                    if ($product != 1) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    public function getListIdByProductId($productId)
    {
        if (!empty($productId)) {
            $listId = $this->where('id', $productId)->findColumn('list_id');
            if (!empty($listId)) {
                return $listId;
            }
        }
        return false;
    }

    public function deleteProductsByListId($listId)
    {
        if (!empty($listId)) {
            $productsId = $this->where('list_id', $listId)->findColumn('id');
            if (!empty($productsId)) {
                $this->delete($productsId);
                return true;
            }
        }
        return false;
    }
}