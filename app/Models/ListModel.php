<?php namespace App\Models;

use CodeIgniter\Model;

class ListModel extends Model
{
    protected $table = 'lists';
    protected $allowedFields = ['list_name', 'user_id', 'is_done'];
    protected $primaryKey = 'id';
    protected $returnType = 'array';
}