<?php namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Model;

class User extends BaseController
{
    public function index()
    {
        $session = session();
        if (!empty($session->get('user'))) {
            return redirect()->to('/list');
        }
        $data = [];
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'email' => 'required|valid_email',
                'password' => 'required|min_length[4]|validateUser[email,password]',
            ];
            $errors = [
                'password' => [
                    'validateUser' => 'Email or Password don\'t match'
                ]
            ];
            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            } else {
                $model = new UserModel();
                $user = $model->where('email', $this->request->getVar('email'))->first();
                $session->set('user', $user);
                $session->setFlashdata('success', 'Successful login');
                return redirect()->to('/list');
            }
        }

        echo view('templates/header', $data);
        echo view('user/login');
        echo view('templates/footer');
    }

    public function register()
    {
        $session = session();
        if (!empty($session->get('user'))) {
            return redirect()->to('/list');
        }
        $userModel = model('App\Models\UserModel');
        $data = [];
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
            if (!$this->validate($userModel->validationRules)) {
                $data['validation'] = $this->validator;
            }else {
                $model = new UserModel();
                $newData = [
                    'user_name' => $this->request->getVar('user_name'),
                    'email' => $this->request->getVar('email'),
                    'password' => $this->request->getVar('password'),
                    'password_confirm' => $this->request->getVar('password_confirm'),
                ];

                $user = $userModel->find($model->insert($newData));
                $session->set('user', $user);
                $session->setFlashdata('success', 'Successful registration');
                return redirect()->to('/list');
            }
        }

        echo view('templates/header', $data);
        echo view('user/register');
        echo view('templates/footer');
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }

    //--------------------------------------------------------------------

}
