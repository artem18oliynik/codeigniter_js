<?php namespace App\Controllers;

use App\Models\ListModel;
use App\Models\ProductModel;

class ListController extends BaseController
{
    public function index()
    {
        helper(['url']);
        $session = session();
        if (!empty($session->get('user'))) {
            $listModel = model('App\Models\ListModel');
            $lists = $listModel->findAll();

            $data['lists'] = $lists;

            echo view('templates/header', $data);
            echo view('list/list');
            echo view('templates/footer');
        } else {
            return redirect()->to('/');
        }
    }

    public function addList()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['listName'])) {
            $dataList = [
                'list_name' => $jsonArray['listName'],
                'user_id' => session()->get('user')['id'],
            ];
            $listModel = new ListModel();
            $list = $listModel->find($listModel->insert($dataList));
            return $this->response->setJSON($list);
        }
    }

    public function getLists()
    {
        $listModel = new ListModel();
        $lists = $listModel->findAll();
        return $this->response->setJSON($lists);
    }

    public function showList()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['listId'])) {
            $productModel = new ProductModel();
            $list = $productModel->where('list_id', $jsonArray['listId'])->findAll();
            return $this->response->setJSON($list);
        }
    }

    public function addProduct()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['productName']) && !empty($jsonArray['listId'])) {
            $dataProduct = [
                'product_name' => $jsonArray['productName'],
                'list_id' => $jsonArray['listId'],
            ];
            $productModel = new ProductModel();
            $product = $productModel->find($productModel->insert($dataProduct));

            // Refresh the list after adding a product
            $listModel = new ListModel();
            $listModel->update($jsonArray['listId'], ['is_done' => 0]);

            return $this->response->setJSON($product);
        }
    }

    public function setPerformed()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['productId']) && isset($jsonArray['setValue'])) {
            $productModel = new ProductModel();
            $data = [
                'performed' => $jsonArray['setValue'],
            ];
            $productModel->update($jsonArray['productId'], $data);

            // Checking products in the list
            $listId = $productModel->getListIdByProductId($jsonArray['productId']);
            $checkProducts = $productModel->checkPerformedByListId($listId);
            $listModel = new ListModel();
            if ($checkProducts) {
                $listModel->update($listId, ['is_done' => 1]);
            } else {
                $listModel->update($listId, ['is_done' => 0]);
            }

            return $this->response->setJSON(true);
        }
        return $this->response->setJSON(false);
    }

    public function deleteProduct()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['productId'])) {
            $productModel = new ProductModel();
            $productModel->delete($jsonArray['productId']);
            return $this->response->setJSON(true);
        }
        return $this->response->setJSON(false);
    }

    public function deleteList()
    {
        $jsonArray = json_decode(file_get_contents('php://input'), true);
        if (!empty($jsonArray['listId'])) {
            $productModel = new ProductModel();
            $productModel->deleteProductsByListId($jsonArray['listId']);
            $listModel = new ListModel();
            $listModel->delete($jsonArray['listId']);
            return $this->response->setJSON(true);
        }
        return $this->response->setJSON(false);
    }
    //--------------------------------------------------------------------

}
