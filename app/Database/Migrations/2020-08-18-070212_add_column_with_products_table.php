<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddColumnWithProductsTable extends Migration
{
	public function up()
	{
        $fields = [
            'performed' => [
                'type' => 'BOOLEAN',
                'default' => 0,
            ]
        ];
        $this->forge->addColumn('products', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropColumn('products', 'performed');
	}
}
