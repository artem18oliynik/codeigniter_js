<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddColumnWithListsTable extends Migration
{
	public function up()
	{
        $fields = [
            'is_done' => [
                'type' => 'BOOLEAN',
                'default' => 0,
            ]
        ];
        $this->forge->addColumn('lists', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropColumn('lists', 'is_done');
	}
}
