<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateListsTable extends Migration
{
	public function up()
	{
        $this->forge->addField(
            [
                'id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => true,
                    'auto_increment' => true,
                ],
                'list_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => '200',
                ],
                'user_id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                ],
            ]
        );
        $this->forge->addKey('id', true);
        $this->forge->createTable('lists');
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->dropTable('lists');
	}
}
