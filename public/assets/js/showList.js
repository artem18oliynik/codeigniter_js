(function () { // document ready
    createlist.onclick = function () {
        // library for custom prompt
        swal("Введите название списка:", {
            content: "input",
        })
            .then((listName) => {
                if (listName != "") {
                    addList(listName);
                } else {
                    swal("Название списка не должно быть пустым!");
                }
            });
    };
})();

// Add new List
async function addList(listName) {
    let response = await fetch('/list/addList', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({listName: listName})
    });
    let result = await response.json();

    document.getElementById('lists').innerHTML += `
        <li class="list-group-item list" listId=${result['id']}>
                                        <div class="todo-indicator bg-warning"></div>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-2">
                                                </div>
                                                <div class="widget-content-left" onclick="showList(${result['id']})">
                                                    <div class="widget-heading">${result['list_name']}
                                                    </div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <button class="border-0 btn-transition btn btn-outline-danger"
                                                        onclick="deleteList(${result['id']})">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
        `;
}

async function showList(listId) {
    let response = await fetch('/list/showList', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({listId: listId})
    });
    let result = await response.json();

    // build page with products
    document.getElementById("card-title").innerHTML = `<i class="fa fa-tasks"></i>&nbsp;Products`;
    document.getElementById('cardfooter').style.display = 'none';

    lists.remove();

    document.getElementById('card-header')
        .insertAdjacentHTML('beforeend',
            `<button type="button" class="btn btn-link" 
                        id="backtolists" onclick="backToLists()">Back to lists
                   </button>`
        );
    document.getElementById('card-header')
        .insertAdjacentHTML('afterend',
            `<div class="input-group mb-3" id="productinput">
                      <input type="text" id="inputProduct" class="form-control" placeholder="Product name" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" id="addproduct" onclick="addProduct()" listId=${listId}>Add</button>
                        </div>
                    </div>`
        );

    let list = document.createElement("ul");
    list.setAttribute("id", "productlist");
    list.setAttribute("class", "list-group list-group-flush");

    for (let key in result) {
        let checked = (result[key].performed == 1) ? "checked" : ""; // set class 'checked'
        list.innerHTML += `<li class="list-group-item ${checked}" listId=${result[key].list_id} productId=${result[key].id}>
                                        <div class="todo-indicator bg-warning"></div>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-2">
                                              
                                                </div>
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">${result[key].product_name}
                                                    </div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <button class="border-0 btn-transition btn btn-outline-danger"
                                                            onclick="deleteProduct(${result[key].id})">
                                                            <i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>`;
    }
    content.appendChild(list);

    //  Add a "checked" symbol when clicking on a list item
    let productlist = document.getElementById('productlist');
    productlist.addEventListener('click', function (ev) {

        if (ev.target.tagName === 'DIV') {
            let element = ev.target.closest('li');
            if (element.classList.contains('checked') != true) {
                element.classList.add('checked');
                let productId = element.getAttribute('productid');
                setPerformedProduct(productId, 1);
            } else {
                element.classList.remove('checked');
                let productId = element.getAttribute('productid');
                setPerformedProduct(productId, 0);
            }
        }
    }, false);

}

// function to set performed value in DB
async function setPerformedProduct(productId, setValue) {
    let response = await fetch('/list/setPerformed', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({productId: productId, setValue: setValue})
    });
    let result = await response.json();
}

// Back to lists
function backToLists() {
    document.getElementById("card-title").innerHTML = `<i class="fa fa-tasks"></i>&nbsp;Lists`;
    document.getElementById('cardfooter').style.display = 'block';
    getLists();
    backtolists.remove();
    productinput.remove();
    productlist.remove();
}

// get all lists
async function getLists() {
    let response = await fetch('/list/getLists', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify()
    });
    let result = await response.json();
    let list = document.createElement("ul");
    list.setAttribute("id", "lists");
    list.setAttribute("class", "list-group list-group-flush");

    for (let key in result) {
        let checked = (result[key].is_done == 1) ? "checked" : ""; // set class 'checked'
        list.innerHTML += `<li class="list-group-item list ${checked}" listId=${result[key].id}>
                                        <div class="todo-indicator bg-warning"></div>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-2">
                                                </div>
                                                <div class="widget-content-left" onclick="showList(${result[key].id})">
                                                    <div class="widget-heading">${result[key].list_name}
                                                    </div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <button class="border-0 btn-transition btn btn-outline-danger"
                                                        onclick="deleteList(${result[key].id})">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>`;
    }
    content.appendChild(list);
}


// Delete a product from the list
async function deleteProduct(productId) {
    let response = await fetch('/list/deleteProduct', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({productId: productId})
    });
    let result = await response.json();

    document.querySelector(`[productid="${productId}"]`).remove();
}

// Delete List
async function deleteList(listId) {
    let response = await fetch('/list/deleteList', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({listId: listId})
    });
    let result = await response.json();
    document.querySelector(`[listid="${listId}"]`).remove();
}

// Add Product
function addProduct() {
    let listId = addproduct.getAttribute('listId');
    let productName = document.getElementById("inputProduct").value;
    if (productName === '') {
        alert("You must write something!");
    } else {
        saveProduct(productName, listId);
    }
    document.getElementById("inputProduct").value = "";
}

// Save product to DB
async function saveProduct(productName, listId) {
    let response = await fetch('/list/addProduct', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({productName: productName, listId: listId})
    });
    let result = await response.json();
    document.getElementById('productlist').innerHTML += `
        <li class="list-group-item" class="list" listId=${result['list_id']} productId=${result['id']}>
                                        <div class="todo-indicator bg-warning"></div>
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left mr-2">
                                                </div>
                                                <div class="widget-content-left" onclick="performedProduct(${result['id']})">
                                                    <div class="widget-heading">${result['product_name']}
                                                    </div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <button class="border-0 btn-transition btn btn-outline-danger"
                                                            onclick="deleteProduct(${result['id']})">
                                                            <i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
        `;
}

